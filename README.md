# Default KDE Look and Feel for RebornOS

## Build and Sign the Package (for Developers)

```
makepkg -g >> PKGBUILD && makepkg
gpg --detach-sign --use-agent {file_name}
```

## Purpose
To improve the default look and feel of the KDE Plasma desktop without adding any packages or bloat the user might not want. Thus, all this package does is modify the default settings for KDE Plasma, giving the user a much more elegant look and feel upon first logging in.

# KDE config file paths (taken from [this lovely Github page](https://github.com/shalva97/kde-configuration-files))

#### Panel
   * `.config/plasma-org.kde.plasma.desktop-appletsrc`

#### Appearance
   * Look and Feel
      * `.config/kcminputrc`
      * `.config/kdeglobals`
      * `.config/kscreenlockerrc`
      * `.config/ksplashrc`
      * `.config/kwinrc`
      * `.config/plasmarc`
   * Workspace Theme
      * Splash Screen
         * `.config/ksplashrc`
      * Plasma Theme
         * `.config/plasmarc`
      * Cursors
         * `.config/kcminputrc`
   * Colors
       * `.config/kdeglobals`
       * `.config/Trolltech.conf`
   * Fonts
      * Fonts
         * `.config/kdeglobals`
         * `.config/kcmfonts`
   * Icons
      * `.config/kdeglobals` [Icons]
   * Application Style
      * `.config/kdeglobals`
      * [Window Decorations](#Window-Decorations)
         * `.config/kwinrc`
#### Workspace
   * Desktop behavior
      * Workspace
         * `.config/plasmarc`
      * Desktop behavior
         * [Screen Edges](#screen-edges)
            * `.config/kwinrc`
   * Window Management
      * Window  behavior
         * `.config/kwnirc`
      * Window Rules
         * `.config/kwinrulesrc`
   * Shortcuts
      * Custom Shortcuts
         * `.config/khotkeys`
      * Global Shortcuts
         * `.config/kglobalshortcutsrc`
   * Startup and Shutdown
      * Background Services
          * `.config/kded5rc`
       * Desktop Session
          * `.config/ksmserverrc`
   * Search
      * Plasma Search
         * `.config/krunnerrc`
      * File Search
         * `.config/baloofilerc`
#### Personalization
   * Account Details
   * Notifications
      * `.config/plasmanotifyrc`
   * Regional Settings
      * Formats
         * `.config/plasma-localerc`
      * Date & Time
         * `.config/ktimezonedrc`
   * Accessibility
      * `.config/kaccessrc`
   * Applications
      * Default Applications
         * `.config/kdeglobals`
#### Network
   * Connections
      * `/etc/NetworkManager/system-connections`
   * Settings
   * Bluetooth
      * `.config/bluedevilglobalrc`
#### Hardware
   * Input Devices
      * Keyboard
         * Hardware
            * `.config/kcminputrc`
         * layout
            * TODO
         * Advanced
            * `.config/kxkbrc`
   * Display and Monitor
      * Gamma
         * `.config/kgammarc`
   * Multimedia
   * Power Management
      * Energy Saving
         * `.config/powermanagementprofilesrc`
   * KDE Connect
      * `.config/kdeconnect`
   * Removable Storage
